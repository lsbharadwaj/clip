from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.http import HttpResponse

@login_required
def showProfile(request):
    context = {'name':'Logging In'}
    return render(request,'UserProfile/test.html', context)

def logUserIn(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
        return HttpResponse('')
    else:
        context = {'name':'Logging In'}
        return render(request,'UserProfile/test.html', context)
